-- 1
SELECT customerName FROM customers
WHERE country = 'Philippines';

-- 2
SELECT contactLastName, contactFirstName FROM customers
WHERE customerName = 'La Rochelle Gifts';

-- 3
SELECT productName, MSRP FROM products
WHERE productName = 'The Titanic';

-- 4
SELECT firstName, lastName FROM employees
WHERE email = 'jfirrelli@classicmodelcars.com';

-- 5
SELECT customerName FROM customers
WHERE state IS NULL;

-- 6
SELECT firstname, lastName, email FROM employees
WHERE lastName = 'Patterson' AND firstName = 'Steve';

-- 7
SELECT customerName, country, creditLimit FROM customers
WHERE country != 'USA' AND creditLimit > 3000;

-- 8
SELECT customerNumber FROM orders
WHERE comments LIKE '%DHL%';

-- 9
SELECT productLine FROM productLines
WHERE textDescription LIKE '%state of the art%';

-- 10
SELECT DISTINCT country FROM customers;

-- 11
SELECT DISTINCT status from orders;

-- 12
SELECT customerName, country FROM customers     
WHERE country IN ('USA', 'France', 'Canada');

-- 13
SELECT employees.firstName, employees.lastName, offices.city  FROM employees
    INNER JOIN offices
        ON employees.officeCode = offices.officeCode
WHERE offices.city = 'Tokyo';

-- 14
SELECT customers.customerName FROM customers
    INNER JOIN employees
        ON customers.salesRepEmployeeNumber = employees.employeeNumber
WHERE employees.firstName = 'Leslie' AND employees.lastName = 'Thompson';

-- 15
SELECT products.productName, customers.customerName FROM customers
    INNER JOIN orders ON customers.customerNumber = orders.customerNumber
        INNER JOIN orderdetails ON orders.orderNumber = orderdetails.orderNumber
            INNER JOIN products ON orderdetails.productCode = products.productCode
WHERE customers.customerName = 'Baane Mini Imports';

-- 16
SELECT employees.firstName, employees.lastName, customers.customerName, offices.country
FROM customers
    INNER JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber
        INNER JOIN offices ON employees.officeCode = offices.officeCode
WHERE customers.country = offices.country;

-- 17
SELECT products.productName, products.quantityInStock FROM products
    INNER JOIN productLines ON products.productLine = productLines.productLine
WHERE productLines.productLine = 'planes' AND products.quantityInStock < 1000;

--18
SELECT customerName FROM customers
WHERE phone LIKE '%+81%';